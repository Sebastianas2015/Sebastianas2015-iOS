//
//  DBManager.h
//  Sebastianas2015
//
//  Created by Helder Moreira on 13/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const table_program = @"program";
static NSString * const table_news = @"news";
static NSString * const table_sponsors = @"sponsors";

@interface DBManager : NSObject

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

+(DBManager*)getInstance;

-(void) reset;

-(void) insertProgramEvent:(long)uuid
                 withTitle:(NSString*)title
           withDescription:(NSString*)description
                 withImage:(NSString*)image
                 withVideo:(NSString*)video
                   withDay:(int)day
                 withMonth:(int)month
                  withHour:(int)hour
               withMinutes:(int)minutes;

-(void) insertNewsEvent:(long)uuid
              withTitle:(NSString*)title
        withDescription:(NSString*)description
              withImage:(NSString*)image
              withVideo:(NSString*)video
                withDay:(int)day
              withMonth:(int)month
               withHour:(int)hour
            withMinutes:(int)minutes
             withNotify:(int)notify;

-(void) insertSponsor:(long)uuid
             withName:(NSString*)name
            withImage:(NSString*)image
             withLink:(NSString*)link
            withOrder:(int)order;

-(void) deleteItem:(long)uuid onTable:(NSString*)table;

-(BOOL) existsItem:(long)uuid onTable:(NSString*)table;

-(BOOL) sameImage:(NSString*)image forItem:(long)uuid onTable:(NSString*)table;

-(NSMutableArray*) getProgram;

-(NSMutableArray*) getNews;

-(NSMutableArray*) getNewsNotify;

-(NSMutableArray*) getSponsors;

-(void) setNewNotified:(long)uuid;

@end
