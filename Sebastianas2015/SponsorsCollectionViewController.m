//
//  SponsorsCollectionViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 16/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "SponsorsCollectionViewController.h"
#import "DBManager.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentDownloader.h"

@interface SponsorsCollectionViewController ()

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSString  *documentsDirectory;
@property (nonatomic, strong) NSArray *sponsors;
@property (nonatomic, strong) ContentDownloader *cDownloader;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (void) startRefresh;
- (void) displayToast:(NSString*) message;

@end

@implementation SponsorsCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.alwaysBounceVertical = YES;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    if([[UIScreen mainScreen] bounds].size.width <= 375.0)
        [self.collectionView setContentInset:UIEdgeInsetsMake(5, 5, 5, 5)];
    else
        [self.collectionView setContentInset:UIEdgeInsetsMake(50, 50, 50, 50)];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }
    
    
    self.cDownloader = [ContentDownloader getInstance];
    self.dbManager = [DBManager getInstance];
    self.sponsors = [self.dbManager getSponsors];
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [paths objectAtIndex:0];
}

- (void) displayToast:(NSString*) message{
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil, nil];
    [toast show];
    
    int duration = 3;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void) startRefresh{
    if ([self.cDownloader isOnline])
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self.cDownloader downloadSponsors];
            self.sponsors = [self.dbManager getSponsors];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.collectionView reloadData];
                [self.refreshControl endRefreshing];
            });
        });
    else{
        [self displayToast:@"Sem ligação à internet..."];
        [self.refreshControl endRefreshing];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.sponsors.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:100];
    UILabel *title = (UILabel *)[cell viewWithTag:101];
    
    NSString *imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, [[self.sponsors objectAtIndex:indexPath.row] objectAtIndex:0]];
    [image setImage:[UIImage imageNamed:imagePath]];
    [title setText:[[self.sponsors objectAtIndex:indexPath.row] objectAtIndex:1]];
    
    cell.clipsToBounds = YES;
    cell.layer.masksToBounds = NO;
    [cell.layer setCornerRadius:20.0f];
    [cell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.layer setShadowOpacity:0.5];
    [cell.layer setShadowRadius:3.0];
    [cell.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[[self.sponsors objectAtIndex:indexPath.row] objectAtIndex:3]]];
}

#pragma mark <UICollectionViewDelegate>
- (IBAction)openSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
