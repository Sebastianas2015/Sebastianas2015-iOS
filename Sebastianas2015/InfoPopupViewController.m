//
//  InfoPopupViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 12/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "InfoPopupViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface InfoPopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *v;

@end

@implementation InfoPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.v.clipsToBounds = YES;
    self.v.layer.masksToBounds = NO;
    [self.v.layer setCornerRadius:30.0f];
    [self.v.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v.layer setShadowOpacity:0.5];
    [self.v.layer setShadowRadius:30.0];
    [self.v.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
