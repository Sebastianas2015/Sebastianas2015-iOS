//
//  NewsInfoViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 17/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "NewsInfoViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface NewsInfoViewController ()

@end

@implementation NewsInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.v.clipsToBounds = YES;
    self.v.layer.masksToBounds = NO;
    [self.v.layer setCornerRadius:20.0f];
    [self.v.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v.layer setShadowOpacity:0.5];
    [self.v.layer setShadowRadius:3.0];
    [self.v.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    [self.titleLabel setText:self.titleTxt];
    UIImage *uiImage = [UIImage imageNamed:self.imagePath];
    [self.imageView setImage:uiImage];
    [self.descriptionText setText:self.descriptionTxt];
    [self.descriptionText setFont:[UIFont systemFontOfSize:16.0f]];
    [self.dateLabel setText:self.date];
    
    if(![self.videoLink isEqualToString:@""]){
        [self.videoButton setImage:[UIImage imageNamed:@"youtube_color.png"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)openVideo:(id)sender {
    if(![self.videoLink isEqualToString:@""])
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.videoLink]];
}

@end
