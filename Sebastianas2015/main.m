//
//  main.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 10/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
