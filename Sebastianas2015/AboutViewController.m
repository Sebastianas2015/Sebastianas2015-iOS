//
//  AboutViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 23/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "AboutViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UIView *v1;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }
    
    self.v1.clipsToBounds = YES;
    self.v1.layer.masksToBounds = NO;
    [self.v1.layer setCornerRadius:30.0f];
    [self.v1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v1.layer setShadowOpacity:0.5];
    [self.v1.layer setShadowRadius:3.0];
    [self.v1.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    UILabel *label = (UILabel *)[self.view viewWithTag:100];
    [label setText:version];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)showSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
- (IBAction)openLink:(id)sender {
    if ([sender tag] == 101) {
        NSString *recipients = @"mailto:sebastianas2015@moreirahelder.com?subject=Sebastianas2015";
        NSString *body = @"&body=Corpo";
        
        NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
        email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
    else if ([sender tag] == 102) {
        NSString *finalString = @"https://www.facebook.com/helder.moreira.391082";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 103) {
        NSString *finalString = @"http://www.moreirahelder.com/";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 104) {
        NSString *finalString = @"https://plus.google.com/u/0/115145959453855010665";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 105) {
        NSString *finalString = @"https://pt.linkedin.com/pub/helder-moreira/b6/71a/442";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
}

@end
