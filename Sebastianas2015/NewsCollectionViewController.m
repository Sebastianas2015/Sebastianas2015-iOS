//
//  NewsCollectionViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 17/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "NewsCollectionViewController.h"
#import "DBManager.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentDownloader.h"
#import "NewsInfoViewController.h"

@interface NewsCollectionViewController ()

@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSString  *documentsDirectory;
@property (nonatomic, strong) NSArray *news;
@property (nonatomic, strong) ContentDownloader *cDownloader;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (void) startRefresh;
- (void) displayToast:(NSString*) message;

@end

@implementation NewsCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    self.cache = [[NSCache alloc] init];
    self.collectionView.alwaysBounceVertical = YES;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    if([[UIScreen mainScreen] bounds].size.width <= 375.0)
        [self.collectionView setContentInset:UIEdgeInsetsMake(5, 5, 5, 5)];
    else
        [self.collectionView setContentInset:UIEdgeInsetsMake(50, 50, 50, 50)];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }
    
    
    self.cDownloader = [ContentDownloader getInstance];
    self.dbManager = [DBManager getInstance];
    self.news = [self.dbManager getNews];
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [paths objectAtIndex:0];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        for (int i = (int)self.news.count - 1; i>=0; i--) {
            NSString *uuid = [[self.news objectAtIndex:i]  objectAtIndex:0];
            NSString *imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, uuid];
            UIImage *origImage = [UIImage imageNamed:imagePath];
            if (origImage) {
                [self.cache setObject:origImage forKey:uuid];
            }
        }
    });
}

- (void) displayToast:(NSString*) message{
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil, nil];
    [toast show];
    
    int duration = 3;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void) startRefresh{
    if ([self.cDownloader isOnline])
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self.cDownloader downloadNews];
            self.news = [self.dbManager getNews];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.collectionView reloadData];
                [self.refreshControl endRefreshing];
            });
        });
    else{
        [self displayToast:@"Sem ligação à internet..."];
        [self.refreshControl endRefreshing];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showNew"]) {
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
        NewsInfoViewController *destViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
        destViewController.titleTxt = [[self.news objectAtIndex:indexPath.row] objectAtIndex:1];
        destViewController.imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, [[self.news objectAtIndex:indexPath.row] objectAtIndex:0]];
        destViewController.descriptionTxt = [[self.news objectAtIndex:indexPath.row] objectAtIndex:2];
        destViewController.videoLink = [[self.news objectAtIndex:indexPath.row] objectAtIndex:4];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *time = [[NSDate alloc] initWithTimeIntervalSince1970:[[[self.news objectAtIndex:indexPath.row] objectAtIndex:6] integerValue]];
        destViewController.date = [dateFormat stringFromDate:time];
        [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.news.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:100];
    UILabel *title = (UILabel *)[cell viewWithTag:101];
    UILabel *date = (UILabel *)[cell viewWithTag:102];
    
    NSString *uuid = [[self.news  objectAtIndex:indexPath.row ] objectAtIndex:0];
    NSString *imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, uuid];
    
    UIImage *origImage = [self.cache objectForKey:uuid];
    
    if(!origImage){
        origImage = [UIImage imageNamed:imagePath];
        if (origImage) {
            [self.cache setObject:origImage forKey:uuid];
        }
    }
    [image setImage:origImage];
    [title setText:[[self.news objectAtIndex:indexPath.row] objectAtIndex:1]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *time = [[NSDate alloc] initWithTimeIntervalSince1970:[[[self.news objectAtIndex:indexPath.row] objectAtIndex:6] integerValue]];
    [date setText:[dateFormat stringFromDate:time]];
    
    cell.clipsToBounds = YES;
    cell.layer.masksToBounds = NO;
    [cell.layer setCornerRadius:20.0f];
    [cell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.layer setShadowOpacity:0.5];
    [cell.layer setShadowRadius:3.0];
    [cell.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>
- (IBAction)openSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
