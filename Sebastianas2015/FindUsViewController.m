//
//  FindUsViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 11/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "FindUsViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>

@import MapKit;

@interface FindUsViewController ()
@property (weak, nonatomic) IBOutlet UIView *v1;
@property (weak, nonatomic) IBOutlet UIView *v2;
@property (weak, nonatomic) IBOutlet UIView *v3;

@end

@implementation FindUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }
    
    self.v1.clipsToBounds = YES;
    self.v1.layer.masksToBounds = NO;
    [self.v1.layer setCornerRadius:30.0f];
    [self.v1.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v1.layer setShadowOpacity:0.5];
    [self.v1.layer setShadowRadius:3.0];
    [self.v1.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    self.v2.clipsToBounds = YES;
    self.v2.layer.masksToBounds = NO;
    [self.v2.layer setCornerRadius:20.0f];
    [self.v2.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v2.layer setShadowOpacity:0.5];
    [self.v2.layer setShadowRadius:3.0];
    [self.v2.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    self.v3.clipsToBounds = YES;
    self.v3.layer.masksToBounds = NO;
    [self.v3.layer setCornerRadius:20.0f];
    [self.v3.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v3.layer setShadowOpacity:0.5];
    [self.v3.layer setShadowRadius:3.0];
    [self.v3.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openURL:(id)sender {
    if ([sender tag] == 0) {
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = 41.288867;
        coordinate.longitude = -8.340558;
        
        MKPlacemark *mPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:mPlacemark];
        [mapItem setName:@"Sebastianas - Freamunde"];
        [mapItem openInMapsWithLaunchOptions:nil];
    }
    else if ([sender tag] == 1) {
        NSString *recipients = @"mailto:sebastianas2015@gmail.com?subject=Sebastianas2015";
        NSString *body = @"&body=Corpo";
        
        NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
        email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
    else if ([sender tag] == 2) {
        NSString *finalString = @"http://www.sebastianas.com/";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 3) {
        NSString *finalString = @"https://www.facebook.com/festassebastianas";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 4) {
        NSString *finalString = @"https://twitter.com/sebastianas";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 5) {
        NSString *finalString = @"https://plus.google.com/+Sebastianas";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 6) {
        NSString *finalString = @"https://www.youtube.com/channel/UC2SbzH94t2vODQCq1UG55KA";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 7) {
        NSString *finalString = @"https://instagram.com/sebastianas/";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
}
- (IBAction)showSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
