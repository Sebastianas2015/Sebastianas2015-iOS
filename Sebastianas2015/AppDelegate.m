//
//  AppDelegate.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 10/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "AppDelegate.h"
#import "ContentDownloader.h"
#import "SWRevealViewController.h"
#import "ContentDownloader.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"NotFirstTime"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NotFirstTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:15 forKey:@"update_interval"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notifications"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"wifi_only"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            ContentDownloader *cDownloader = [ContentDownloader getInstance];
            [cDownloader updateAll];
        });
    }
    
    long interval = [[NSUserDefaults standardUserDefaults] integerForKey:@"update_interval"];
    [application setMinimumBackgroundFetchInterval:interval*60];
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound | UIUserNotificationTypeBadge categories:nil]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    ContentDownloader *cDownloader = [ContentDownloader getInstance];
    [cDownloader fetchNewDataWithCompletionHandler:^(UIBackgroundFetchResult result) {
        completionHandler(result);
    }];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * newsController = [storyboard instantiateViewControllerWithIdentifier:@"NEWSC"];
    
    SWRevealViewController *revealController = (SWRevealViewController*) self.window.rootViewController;
    [revealController pushFrontViewController:newsController animated:YES];
}
@end
