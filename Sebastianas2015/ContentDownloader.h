//
//  ContentDownloader.h
//  Sebastianas2015
//
//  Created by Helder Moreira on 13/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ContentDownloader : NSObject

+ (ContentDownloader*) getInstance;

- (void) downloadMain;

- (void) downloadProgram;

- (void) downloadNews;

- (void) downloadSponsors;

- (void) buildNotifications;

- (BOOL) imageExists:(long) uuid;

- (void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

- (bool) isOnline;

- (void) updateAll;

@end
