//
//  ProgramCollectionViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 17/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "ProgramCollectionViewController.h"
#import "SWRevealViewController.h"
#import "DBManager.h"
#import "ContentDownloader.h"
#import "NewsInfoViewController.h"

@interface ProgramCollectionViewController ()

@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSString  *documentsDirectory;
@property (nonatomic, strong) NSArray *program;
@property (nonatomic, strong) ContentDownloader *cDownloader;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (void) startRefresh;
- (void) displayToast:(NSString*) message;
- (NSArray*) getProgramSorted;

@end

@implementation ProgramCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectory = [paths objectAtIndex:0];
    
    self.cache = [[NSCache alloc] init];
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(20, 0, 20, 0);
    
    self.collectionView.alwaysBounceVertical = YES;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    if([[UIScreen mainScreen] bounds].size.width <= 375.0)
        [self.collectionView setContentInset:UIEdgeInsetsMake(5, 5, 5, 5)];
    else
        [self.collectionView setContentInset:UIEdgeInsetsMake(50, 50, 50, 50)];

    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }
    
    self.cDownloader = [ContentDownloader getInstance];
    self.dbManager = [DBManager getInstance];
    self.program = [self getProgramSorted];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"NotProgramFirstTime"]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NotProgramFirstTime"];
        [NSThread sleepForTimeInterval:2.0f];
        [self startRefresh];
        [self.refreshControl beginRefreshing];
    }
}

- (NSArray*) getProgramSorted{
    NSArray *program = [self.dbManager getProgram];
    
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        for (int i = (int)program.count - 1; i>=0; i--) {
            NSString *uuid = [[program objectAtIndex:i]  objectAtIndex:0];
            NSString *imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, uuid];
            UIImage *origImage = [UIImage imageNamed:imagePath];
            if (origImage) {
                [self.cache setObject:origImage forKey:uuid];
            }
        }
    });
    
    int lastDay = 3;
    NSMutableArray *programSections = [[NSMutableArray alloc] init];
    NSMutableArray *dayProgram = [[NSMutableArray alloc] init];
    for (int i = 0; i < program.count; i++){
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[[[program objectAtIndex:i] objectAtIndex:5] integerValue]];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
        
        if ((int)[components day] != lastDay){
            lastDay = (int)[components day];
            [programSections addObject:[dayProgram copy]];
            [dayProgram removeAllObjects];
        }
        [dayProgram addObject:[program objectAtIndex:i]];
    }
    if(dayProgram.count != 0)
        [programSections addObject:[dayProgram copy]];
    [dayProgram removeAllObjects];
    
    
    NSMutableArray *sortedSections = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<programSections.count; i++) {
        NSArray *sorted = [[programSections objectAtIndex:i] sortedArrayUsingComparator:^(id obj1, id obj2){
            NSArray *a1 = obj1;
            NSArray *a2 = obj2;
            
            NSDate *date1 = [[NSDate alloc] initWithTimeIntervalSince1970:[[a1 objectAtIndex:5] integerValue]];
            NSDate *date2 = [[NSDate alloc] initWithTimeIntervalSince1970:[[a2 objectAtIndex:5] integerValue]];
            
            int hour1 = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:date1] hour];
            int hour2 = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:date2] hour];
            int minute1 = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:date1] minute];
            int minute2 = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:date2] minute];
            
            if(hour1 <= 8)
                hour1 += 24;
            if(hour2 <= 8)
                hour2 += 24;
            
            if(hour1 > hour2)
                return (NSComparisonResult)NSOrderedDescending;
            
            if (hour2 > hour1)
                return (NSComparisonResult)NSOrderedAscending;
            
            if(minute1 > minute2)
                return (NSComparisonResult)NSOrderedDescending;
            
            if (minute2 > minute1)
                return (NSComparisonResult)NSOrderedAscending;
            
            return (NSComparisonResult)NSOrderedSame;
            
        }];
        [sortedSections addObject:sorted];
    }
    [programSections removeAllObjects];
    return sortedSections;
}

- (void) displayToast:(NSString*) message{
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil, nil];
    [toast show];
    
    int duration = 3;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}

- (void) startRefresh{
    if ([self.cDownloader isOnline])
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self.cDownloader downloadProgram];
            self.program = [self getProgramSorted];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.collectionView reloadData];
                [self.refreshControl endRefreshing];
            });
        });
    else{
        [self displayToast:@"Sem ligação à internet..."];
        [self.refreshControl endRefreshing];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showNew"]) {
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
        NewsInfoViewController *destViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
        destViewController.titleTxt = [[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:1];
        destViewController.imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, [[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:0]];
        destViewController.descriptionTxt = [[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:2];
        destViewController.videoLink = [[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:4];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *time = [[NSDate alloc] initWithTimeIntervalSince1970:[[[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:5] integerValue]];
        destViewController.date = [dateFormat stringFromDate:time];
        [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.program.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return ((NSArray*)[self.program objectAtIndex:section]).count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:102];
    UILabel *title = (UILabel *)[cell viewWithTag:101];
    UILabel *date = (UILabel *)[cell viewWithTag:103];
    NSString *uuid = [[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:0];
    NSString *imagePath = [NSString stringWithFormat: @"%@/%@", self.documentsDirectory, uuid];
    
    UIImage *origImage = [self.cache objectForKey:uuid];
    
    if(!origImage){
        origImage = [UIImage imageNamed:imagePath];
        if (origImage) {
            [self.cache setObject:origImage forKey:uuid];
        }
    }
    
    [image setImage:origImage];
    [title setText:[[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:1]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSDate *time = [[NSDate alloc] initWithTimeIntervalSince1970:[[[[self.program objectAtIndex:indexPath.section] objectAtIndex:indexPath.row ] objectAtIndex:5] integerValue]];
    NSString *dateText = [dateFormat stringFromDate:time];
    if(![dateText isEqualToString:@"08:00"])
        [date setText:[dateFormat stringFromDate:time]];
    else
        [date setText:@""];
    
    cell.clipsToBounds = YES;
    cell.layer.masksToBounds = NO;
    [cell.layer setCornerRadius:20.0f];
    [cell.layer setShadowColor:[UIColor blackColor].CGColor];
    [cell.layer setShadowOpacity:0.5];
    [cell.layer setShadowRadius:3.0];
    [cell.layer setShadowOffset:CGSizeMake(0.0, 3.0)];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        NSArray *events = [self.program objectAtIndex:indexPath.section];
        NSArray *event = [events objectAtIndex:0];
        NSString *dateText = [event objectAtIndex:5];
        
        UILabel *title = (UILabel *)[headerView viewWithTag:200];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:[dateText integerValue]];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE"];
        NSString *titleText = [[NSString alloc]initWithFormat:@"%@%@ - Dia %d de Julho", [[[dateFormatter stringFromDate:date] substringToIndex:1] uppercaseString], [[dateFormatter stringFromDate:date] substringFromIndex:1],(int)[components day]];
        title.text = titleText;
        
        reusableview = headerView;
    }
    
    return reusableview;
}

#pragma mark <UICollectionViewDelegate>
- (IBAction)openSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
