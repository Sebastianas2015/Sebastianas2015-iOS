//
//  StreamViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 11/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "StreamViewController.h"
#import "SWRevealViewController.h"
#import "ContentDownloader.h"
#import <QuartzCore/QuartzCore.h>


BOOL streamOnline = NO;

@interface StreamViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *v;
@property (weak, nonatomic) IBOutlet UIButton *btnStream;
@property (weak, nonatomic) IBOutlet UILabel *labelOnline;
@property (nonatomic, strong) ContentDownloader *cDownloader;

@end

@implementation StreamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.rearViewRevealWidth = 310;
    }

    [self.v.layer setCornerRadius:25.0f];
    [self.v.layer setMasksToBounds:YES];
    self.view.clipsToBounds = YES;
    
    self.cDownloader = [ContentDownloader getInstance];
    
    [self.cDownloader downloadMain];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *streamOnlineDF = @"streamOnline";
    
    if ([preferences objectForKey:streamOnlineDF] != nil)
    {
        streamOnline = [preferences boolForKey:streamOnlineDF];
        if (streamOnline){
            UIImage *btnImage = [UIImage imageNamed:@"youtube_color.png"];
            [self.btnStream setImage:btnImage forState:UIControlStateNormal];
            [self.labelOnline setText:@"Online"];
        } else{
            UIImage *btnImage = [UIImage imageNamed:@"youtube_bw.png"];
            [self.btnStream setImage:btnImage forState:UIControlStateNormal];
            [self.labelOnline setText:@"Offline"];
        }
    }
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)showSettings:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
- (IBAction)openStream:(id)sender {
    if (streamOnline){
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *streamLinkDF = @"streamLink";
        NSString *streamLink = [preferences stringForKey:streamLinkDF];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:streamLink]];
    }
}

- (void)testFunction{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSString *streamOnlineDF = @"streamOnline";
    NSString *streamLinkDF = @"streamLink";
    
    const BOOL streamOnline = YES;
    const NSString *streamLink = @"https://www.youtube.com/watch?v=xfLmmRS7eiY";
    [preferences setBool:streamOnline forKey:streamOnlineDF];
    [preferences setObject:streamLink forKey:streamLinkDF];
    
    const BOOL didSave = [preferences synchronize];
    if (!didSave)
    {
        NSLog(@"Error writting");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
