//
//  FindUsViewController.h
//  Sebastianas2015
//
//  Created by Helder Moreira on 11/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end
