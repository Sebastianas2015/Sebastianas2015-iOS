//
//  DBManager.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 13/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>


@interface DBManager()

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

-(void)copyDatabaseIntoDocumentsDirectory;
-(NSDate*)getDate:(int)year withDay:(int)day withMonth:(int)month withHour:(int)hour withMinutes:(int)minutes;

@end

static DBManager *dbManager = nil;

@implementation DBManager

+(DBManager*)getInstance{
    if (dbManager == nil){
        dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sebsDB.sql"];
    }
    return dbManager;
}

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        self.databaseFilename = dbFilename;
        [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}

-(void)copyDatabaseIntoDocumentsDirectory{
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

-(void) reset{
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"delete from %@", table_program];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        if (sqlite3_step(query) != SQLITE_DONE) {
            NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
        }
        sqlite3_reset(query);
        queryText = [NSString stringWithFormat:@"delete from %@", table_news];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        if (sqlite3_step(query) != SQLITE_DONE) {
            NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
        }
        sqlite3_reset(query);
        queryText = [NSString stringWithFormat:@"delete from %@", table_sponsors];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        if (sqlite3_step(query) != SQLITE_DONE) {
            NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}
-(void) insertProgramEvent:(long)uuid
                 withTitle:(NSString*)title
           withDescription:(NSString*)description
                 withImage:(NSString*)image
                 withVideo:(NSString*)video
                   withDay:(int)day
                 withMonth:(int)month
                  withHour:(int)hour
               withMinutes:(int)minutes{
    
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText;
        if (![self existsItem:uuid onTable:table_program]){
            queryText = [NSString stringWithFormat:@"insert into %@ values(?, ?, ?, ?, ?, ?)",table_program];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_int64(query, 1, uuid);
                sqlite3_bind_text(query, 2, [title UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [description UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 4, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 5, [video UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 6, (int)[self getDate:2015 withDay:day withMonth:month withHour:hour withMinutes:minutes].timeIntervalSince1970);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
            
        } else{
            queryText = [NSString stringWithFormat:@"update %@ set title = ?, description = ?, image = ?, video = ?, event_date = ? where id = ?",table_program];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_text(query, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 4, [video UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 5, (int)[self getDate:2015 withDay:day withMonth:month withHour:hour withMinutes:minutes].timeIntervalSince1970);
                sqlite3_bind_int64(query, 6, uuid);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}

-(void) insertNewsEvent:(long)uuid
              withTitle:(NSString*)title
        withDescription:(NSString*)description
              withImage:(NSString*)image
              withVideo:(NSString*)video
                withDay:(int)day
              withMonth:(int)month
               withHour:(int)hour
            withMinutes:(int)minutes
             withNotify:(int)notify{
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText;
        if (![self existsItem:uuid onTable:table_news]){
            queryText = [NSString stringWithFormat:@"insert into %@ values(?, ?, ?, ?, ?, ?, ?)",table_news];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_int64(query, 1, uuid);
                sqlite3_bind_text(query, 2, [title UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [description UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 4, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 5, [video UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 7, (int)[self getDate:2015 withDay:day withMonth:month withHour:hour withMinutes:minutes].timeIntervalSince1970);
                sqlite3_bind_int(query, 6, notify);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
        } else{
            queryText = [NSString stringWithFormat:@"update %@ set title = ?, description = ?, image = ?, video = ?, event_date = ? where id = ?",table_news];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_text(query, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 4, [video UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 5, (int)[self getDate:2015 withDay:day withMonth:month withHour:hour withMinutes:minutes].timeIntervalSince1970);
                sqlite3_bind_int64(query, 6, uuid);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}

-(void) insertSponsor:(long)uuid
             withName:(NSString*)name
            withImage:(NSString*)image
             withLink:(NSString*)link
            withOrder:(int)order{
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText;
        if (![self existsItem:uuid onTable:table_sponsors]){
            queryText = [NSString stringWithFormat:@"insert into %@ values(?, ?, ?, ?, ?)",table_sponsors];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_int64(query, 1, uuid);
                sqlite3_bind_text(query, 2, [name UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 4, [link UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 5, order);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
            
        } else{
            queryText = [NSString stringWithFormat:@"update %@ set name = ?, image = ?, link = ?, p_order = ? where id = ?",table_sponsors];
            if ((sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL)) == SQLITE_OK){
                
                sqlite3_bind_text(query, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 2, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(query, 3, [link UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(query, 4, order);
                sqlite3_bind_int64(query, 5, uuid);
                
                if (sqlite3_step(query) != SQLITE_DONE) {
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            } else {
                NSLog(@"Error preparing statement.");
            }
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}

-(void) deleteItem:(long)uuid onTable:(NSString*)table{
    
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"delete from %@ where id = ?", table];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        sqlite3_bind_int64(query, 1, uuid);
        if (sqlite3_step(query) != SQLITE_DONE) {
            NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}

-(BOOL) existsItem:(long)uuid onTable:(NSString*)table{
    BOOL ret = NO;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select id from %@ where id = ?", table];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        sqlite3_bind_int64(query, 1, uuid);
        
        if(sqlite3_step(query) == SQLITE_ROW) {
            char *result = (char *)sqlite3_column_text(query, 0);
            if (result != NULL) {
                ret = YES;
            }
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    return ret;
}

-(BOOL) sameImage:(NSString*)image forItem:(long)uuid onTable:(NSString*)table{
    
    BOOL ret = NO;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select image from %@ where id = ?", table];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        sqlite3_bind_int64(query, 1, uuid);
        
        if(sqlite3_step(query) == SQLITE_ROW) {
            char *result = (char *)sqlite3_column_text(query, 0);
            if (result != NULL) {
                ret = [[NSString stringWithUTF8String:result] isEqualToString:image];
            }
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    return ret;
}

-(NSArray*) getProgram{
    NSMutableArray *results = nil;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select * from %@ order by event_date", table_program];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        
        results = [[NSMutableArray alloc] init];
        NSMutableArray *arrDataRow;
        while(sqlite3_step(query) == SQLITE_ROW) {
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(query);
            for (int i=0; i<totalColumns; i++){
                char *dbDataAsChars = (char *)sqlite3_column_text(query, i);
                if (dbDataAsChars != NULL) {
                    [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                }
            }
            if (arrDataRow.count > 0) {
                [results addObject:arrDataRow];
            }
        }
        
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    
    NSArray *ret = [NSArray arrayWithArray:results];

    if(results != nil)
        [results removeAllObjects];
    
    return ret;
}

-(NSArray*) getNews{
    NSMutableArray *results = nil;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select * from %@ order by event_date desc", table_news];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        
        results = [[NSMutableArray alloc] init];
        NSMutableArray *arrDataRow;
        while(sqlite3_step(query) == SQLITE_ROW) {
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(query);
            for (int i=0; i<totalColumns; i++){
                char *dbDataAsChars = (char *)sqlite3_column_text(query, i);
                if (dbDataAsChars != NULL) {
                    [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                }
            }
            if (arrDataRow.count > 0) {
                [results addObject:arrDataRow];
            }
        }
        
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    
    NSArray *ret = [NSArray arrayWithArray:results];
    
    if(results != nil)
        [results removeAllObjects];
    
    return ret;
}

-(NSArray*) getNewsNotify{
    NSMutableArray *results = nil;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select * from %@ where ? >= event_date  and notified == 1", table_news];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        sqlite3_bind_int(query, 1, [[NSDate date] timeIntervalSince1970]);
        
        results = [[NSMutableArray alloc] init];
        NSMutableArray *arrDataRow;
        while(sqlite3_step(query) == SQLITE_ROW) {
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(query);
            for (int i=0; i<totalColumns; i++){
                char *dbDataAsChars = (char *)sqlite3_column_text(query, i);
                if (dbDataAsChars != NULL) {
                    [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                }
            }
            if (arrDataRow.count > 0) {
                [results addObject:arrDataRow];
            }
        }
        
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    
    NSArray *ret = [NSArray arrayWithArray:results];
    
    if(results != nil)
        [results removeAllObjects];
    
    return ret;
}

-(NSArray*) getSponsors{
    NSMutableArray *results = nil;
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"select * from %@ order by p_order", table_sponsors];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        
        results = [[NSMutableArray alloc] init];
        NSMutableArray *arrDataRow;
        while(sqlite3_step(query) == SQLITE_ROW) {
            arrDataRow = [[NSMutableArray alloc] init];
            int totalColumns = sqlite3_column_count(query);
            for (int i=0; i<totalColumns; i++){
                char *dbDataAsChars = (char *)sqlite3_column_text(query, i);
                if (dbDataAsChars != NULL) {
                    [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                }
            }
            if (arrDataRow.count > 0) {
                [results addObject:arrDataRow];
            }
        }
        
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
    
    NSArray *ret = [NSArray arrayWithArray:results];
    
    if(results != nil)
        [results removeAllObjects];
    
    return ret;
}

-(void) setNewNotified:(long)uuid{
    sqlite3 *sqlite3Database;
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        sqlite3_stmt *query;
        NSString *queryText = [NSString stringWithFormat:@"update %@ set notified = 0 where id = ?", table_news];
        sqlite3_prepare_v2(sqlite3Database, [queryText UTF8String], -1, &query, NULL);
        sqlite3_bind_int64(query, 1, uuid);
        if (sqlite3_step(query) != SQLITE_DONE) {
            NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
        }
        sqlite3_finalize(query);
    }
    sqlite3_close(sqlite3Database);
}

-(NSDate*)getDate:(int)year withDay:(int)day withMonth:(int)month withHour:(int)hour withMinutes:(int)minutes{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    [components setHour:hour];
    [components setMinute:minutes];
    [components setSecond:0];
    [components setNanosecond:0];
    return [calendar dateFromComponents:components];
}

@end
