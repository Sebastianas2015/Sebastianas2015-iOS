//
//  NewsInfoViewController.h
//  Sebastianas2015
//
//  Created by Helder Moreira on 17/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionText;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *v;

@property (weak, nonatomic) NSString *titleTxt;
@property (weak, nonatomic) NSString *imagePath;
@property (weak, nonatomic) NSString *descriptionTxt;
@property (weak, nonatomic) NSString *videoLink;
@property (weak, nonatomic) NSString *date;

@end
