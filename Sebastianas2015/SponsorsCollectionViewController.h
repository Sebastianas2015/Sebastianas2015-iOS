//
//  SponsorsCollectionViewController.h
//  Sebastianas2015
//
//  Created by Helder Moreira on 16/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorsCollectionViewController : UICollectionViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end
