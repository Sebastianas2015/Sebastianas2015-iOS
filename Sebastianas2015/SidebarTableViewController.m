//
//  SidebarTableViewController.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 10/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "SidebarTableViewController.h"



@interface SidebarTableViewController ()

@end

@implementation SidebarTableViewController

    NSArray *menuItems;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)openURL:(id)sender {
    if ([sender tag] == 1) {
        NSString *finalString = @"http://www.fibramunde.pt/";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
    else if ([sender tag] == 2) {
        NSString *finalString = @"http://www.asvs.pt/en/";
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:finalString]];
    }
}

@end
