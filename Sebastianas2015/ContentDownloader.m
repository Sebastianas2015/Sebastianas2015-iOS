//
//  ContentDownloader.m
//  Sebastianas2015
//
//  Created by Helder Moreira on 13/06/15.
//  Copyright (c) 2015 Helder Moreira. All rights reserved.
//

#import "ContentDownloader.h"
#import "DBManager.h"
#import "TBXML.h"
#import "Reachability.h"

@interface ContentDownloader()

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSString  *documentsDirectory;
@property (nonatomic) bool firstTime;

- (void) checkImage:(NSString*)image forUUID:(long) uuid onTable:(NSString*)table;
- (void) deleteImage:(long)uuid;
- (void) downloadImage:(long)uuid withLink:(NSString*)image;
- (int) getNetworkStatus;

@end

static ContentDownloader *cDownloader = nil;

@implementation ContentDownloader

+ (ContentDownloader*) getInstance{
    if (cDownloader == nil){
        cDownloader = [[ContentDownloader alloc] init];
    }
    return cDownloader;
}

- (id) init
{
    self = [super init];
    if (self) {
        self.dbManager = [DBManager getInstance];
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"NotFirstTimeNotify"]){
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NotFirstTimeNotify"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.firstTime = YES;
        } else{
            self.firstTime = NO;
        }
    }
    return self;
}

- (bool) isOnline{
    return ! [self getNetworkStatus] == NotReachable;
}

- (void) updateAll{
    if ([self isOnline]) {
        [self downloadMain];
        [self downloadProgram];
        [self downloadNews];
        [self downloadSponsors];
        [self buildNotifications];
    }
}

-(void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    if ([self isOnline]) {
        [self downloadMain];
        [self downloadProgram];
        [self downloadNews];
        [self downloadSponsors];
        [self buildNotifications];
        completionHandler(UIBackgroundFetchResultNewData);
        NSLog(@"New data was fetched.");
    }
    else{
        completionHandler(UIBackgroundFetchResultFailed);
        NSLog(@"Failed to fetch new data. No internet connection");
    }
}

- (void) downloadMain{
    NSURL *myUrl = [NSURL URLWithString:@"http://www.sebastianas.com/sbt_ext/main.xml"];
    NSData *myData = [NSData dataWithContentsOfURL:myUrl];
    
    TBXML *sourceXML = [[TBXML alloc] initWithXMLData:myData error:nil];
    
    TBXMLElement *rootElement = sourceXML.rootXMLElement;
    TBXMLElement *entryElement = [TBXML childElementNamed:@"settings" parentElement:rootElement];
    
    TBXMLElement *textElement = [TBXML childElementNamed:@"streamLink" parentElement:entryElement];
    NSString *streamLink = [TBXML textForElement:textElement];
    textElement = [TBXML childElementNamed:@"streamOnline" parentElement:entryElement];
    NSString *streamOnline = [TBXML textForElement:textElement];
    BOOL streamOn = ([streamOnline  isEqual: @"true"]);
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    [preferences setBool:streamOn forKey:@"streamOnline"];
    [preferences setObject:streamLink forKey:@"streamLink"];
    NSLog(@"Updated settings.");
}

- (void) downloadProgram{
    NSURL *myUrl = [NSURL URLWithString:@"http://www.sebastianas.com/sbt_ext/program.xml"];
    NSData *myData = [NSData dataWithContentsOfURL:myUrl];
    
    TBXML *sourceXML = [[TBXML alloc] initWithXMLData:myData error:nil];
    
    TBXMLElement *rootElement = sourceXML.rootXMLElement;
    TBXMLElement *entryElement = [TBXML childElementNamed:@"event" parentElement:rootElement];
    
    do {
        if ([[TBXML elementName:entryElement] isEqualToString:@"event"]){
            TBXMLElement *textElement = [TBXML childElementNamed:@"uuid" parentElement:entryElement];
            long uuid = (long)[[TBXML textForElement:textElement] longLongValue];
            textElement = [TBXML childElementNamed:@"title" parentElement:entryElement];
            NSString *title = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"description" parentElement:entryElement];
            NSString *description = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"image" parentElement:entryElement];
            NSString *image = [TBXML textForElement:textElement];
            [self checkImage:image forUUID:uuid onTable:table_program];
            textElement = [TBXML childElementNamed:@"video" parentElement:entryElement];
            NSString *video = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"day" parentElement:entryElement];
            int day = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"month" parentElement:entryElement];
            int month = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"hour" parentElement:entryElement];
            int hour = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"minutes" parentElement:entryElement];
            int minutes = [[TBXML textForElement:textElement] intValue];
            
            [self.dbManager insertProgramEvent:uuid withTitle:title withDescription:description withImage:image withVideo:video withDay:day withMonth:month withHour:hour withMinutes:minutes];
        } else if ([[TBXML elementName:entryElement] isEqualToString:@"deleted_events"]){
            TBXMLElement *deletedEvent = [TBXML childElementNamed:@"deleted_event" parentElement:entryElement];
            if (deletedEvent != nil){
                do{
                    long uuid = (long)[[TBXML textForElement:deletedEvent] longLongValue];
                    [self.dbManager deleteItem:uuid onTable:table_program];
                }while ((deletedEvent = deletedEvent->nextSibling) != nil);
            }
        }
        
    } while ((entryElement = entryElement->nextSibling) != nil);
    NSLog(@"Updated program.");
}

- (void) downloadNews{
    NSURL *myUrl = [NSURL URLWithString:@"http://www.sebastianas.com/sbt_ext/news.xml"];
    NSData *myData = [NSData dataWithContentsOfURL:myUrl];
    
    TBXML *sourceXML = [[TBXML alloc] initWithXMLData:myData error:nil];
    
    TBXMLElement *rootElement = sourceXML.rootXMLElement;
    TBXMLElement *entryElement = [TBXML childElementNamed:@"event" parentElement:rootElement];
    
    do {
        if ([[TBXML elementName:entryElement] isEqualToString:@"event"]){
            TBXMLElement *textElement = [TBXML childElementNamed:@"uuid" parentElement:entryElement];
            long uuid = (long)[[TBXML textForElement:textElement] longLongValue];
            textElement = [TBXML childElementNamed:@"title" parentElement:entryElement];
            NSString *title = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"description" parentElement:entryElement];
            NSString *description = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"image" parentElement:entryElement];
            NSString *image = [TBXML textForElement:textElement];
            [self checkImage:image forUUID:uuid onTable:table_news];
            textElement = [TBXML childElementNamed:@"video" parentElement:entryElement];
            NSString *video = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"day" parentElement:entryElement];
            int day = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"month" parentElement:entryElement];
            int month = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"hour" parentElement:entryElement];
            int hour = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"minutes" parentElement:entryElement];
            int minutes = [[TBXML textForElement:textElement] intValue];
            textElement = [TBXML childElementNamed:@"notify" parentElement:entryElement];
            int notify = ([[TBXML textForElement:textElement]  isEqual: @"true"]) ? 1 : 0;
            
            [self.dbManager insertNewsEvent:uuid withTitle:title withDescription:description withImage:image withVideo:video withDay:day withMonth:month withHour:hour withMinutes:minutes withNotify:notify];
        } else if ([[TBXML elementName:entryElement] isEqualToString:@"deleted_events"]){
            TBXMLElement *deletedEvent = [TBXML childElementNamed:@"deleted_event" parentElement:entryElement];
            if (deletedEvent != nil){
                do{
                    long uuid = (long)[[TBXML textForElement:deletedEvent] longLongValue];
                    [self.dbManager deleteItem:uuid onTable:table_news];
                }while ((deletedEvent = deletedEvent->nextSibling) != nil);
            }
            
            
        }
        
    } while ((entryElement = entryElement->nextSibling) != nil);
    NSLog(@"Updated news.");
}

- (void) downloadSponsors{
    NSURL *myUrl = [NSURL URLWithString:@"http://www.sebastianas.com/sbt_ext/sponsors.xml"];
    NSData *myData = [NSData dataWithContentsOfURL:myUrl];
    
    TBXML *sourceXML = [[TBXML alloc] initWithXMLData:myData error:nil];
    
    TBXMLElement *rootElement = sourceXML.rootXMLElement;
    TBXMLElement *entryElement = [TBXML childElementNamed:@"event" parentElement:rootElement];
    
    int i = 0;
    
    do {
        if ([[TBXML elementName:entryElement] isEqualToString:@"event"]){
            TBXMLElement *textElement = [TBXML childElementNamed:@"uuid" parentElement:entryElement];
            long uuid = (long)[[TBXML textForElement:textElement] longLongValue];
            textElement = [TBXML childElementNamed:@"title" parentElement:entryElement];
            NSString *title = [TBXML textForElement:textElement];
            textElement = [TBXML childElementNamed:@"image" parentElement:entryElement];
            NSString *image = [TBXML textForElement:textElement];
            [self checkImage:image forUUID:uuid onTable:table_sponsors];
            textElement = [TBXML childElementNamed:@"link" parentElement:entryElement];
            NSString *link = [TBXML textForElement:textElement];
            
            [self.dbManager insertSponsor:uuid withName:title withImage:image withLink:link withOrder:i];
        } else if ([[TBXML elementName:entryElement] isEqualToString:@"deleted_sponsors"]){
            TBXMLElement *deletedEvent = [TBXML childElementNamed:@"deleted_sponsor" parentElement:entryElement];
            if (deletedEvent != nil){
                do{
                    long uuid = (long)[[TBXML textForElement:deletedEvent] longLongValue];
                    [self.dbManager deleteItem:uuid onTable:table_sponsors];
                }while ((deletedEvent = deletedEvent->nextSibling) != nil);
            }
            
            
        }
        i++;
    } while ((entryElement = entryElement->nextSibling) != nil);
    NSLog(@"Updated sponsors.");
}

- (void) buildNotifications{
    NSArray *data = [self.dbManager getNewsNotify];
       
    if (data.count == 0) {
        NSLog(@"Nothing to notify");
        return;
    }
    
    bool notify = [[NSUserDefaults standardUserDefaults] boolForKey:@"notifications"];
    UILocalNotification *notifications[data.count];
    
    for (int i = 0; i < data.count; i++) {
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:(int)[[[data objectAtIndex:i] objectAtIndex:6] integerValue]];
        NSString *title = [[data objectAtIndex:i] objectAtIndex:1];
        
        
        NSLog(@"Time interval since now: %f", [[NSDate date] timeIntervalSinceNow]);
        
        if ([date timeIntervalSinceNow] < 0.0) {
            date = [NSDate dateWithTimeIntervalSinceNow:5];
        }
        
        if(notify){
            notifications[i] = [[UILocalNotification alloc]init];
            notifications[i].repeatInterval = NSCalendarUnitDay;
            [notifications[i] setAlertBody:title];
            [notifications[i] setFireDate:date];
            [notifications[i] setTimeZone:[NSTimeZone  systemTimeZone]];
            [notifications[i] setSoundName:UILocalNotificationDefaultSoundName];
            notifications[i].applicationIconBadgeNumber = i + 1;
        }
        [self.dbManager setNewNotified:(long)[[[data objectAtIndex:i] objectAtIndex:0] longLongValue]];
    }
    if (!self.firstTime){
        NSLog(@"Building Notifications");
        [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObjects:notifications count:data.count]];
    }
}

- (void) checkImage:(NSString*)image forUUID:(long) uuid onTable:(NSString*)table{
    if(![self.dbManager sameImage:image forItem:uuid onTable:table]){
        [self deleteImage:uuid];
    }
    if([image isEqualToString:@""])
        return;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"wifi_only"] && (![self getNetworkStatus] == ReachableViaWiFi))
        return;
    bool image_exists = [self imageExists:uuid];
    if(!image_exists)
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self downloadImage:uuid withLink:image];
        });
}

- (void) deleteImage:(long)uuid{
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", self.documentsDirectory,[NSString stringWithFormat:@"%ld",uuid]] error:&error];
}

- (void) downloadImage:(long)uuid withLink:(NSString*)stringURL{
    NSURL  *url = [NSURL URLWithString:stringURL];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", self.documentsDirectory,[NSString stringWithFormat:@"%ld",uuid]];
        if([urlData writeToFile:filePath atomically:YES])
            NSLog(@"Downloaded image with UUID: %ld", uuid);
        else
            NSLog(@"Error downloading image with UUID: %ld", uuid);
        
    }
    else
        NSLog(@"Error downloading image with UUID: %ld - Invalid link", uuid);
}

- (BOOL) imageExists:(long)uuid{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *pathForFile = [NSString stringWithFormat:@"%@/%@", self.documentsDirectory, [NSString stringWithFormat:@"%ld",uuid]];
    return [fileManager fileExistsAtPath:pathForFile];
}

- (int) getNetworkStatus{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    return [reachability currentReachabilityStatus];
}

@end
